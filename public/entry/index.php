<?php
session_start();
include_once "../csrf.php";

//POSTでアクセス
if($_SERVER["REQUEST_METHOD"] == "POST"){
  //POST値の受け取り
  //$email = $_POST['email'];
  //$name  = $_POST['name'];
  //$body  = $_POST['body'];
  $email           = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_SPECIAL_CHARS);
  $name            = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_SPECIAL_CHARS);
  $course          = filter_input(INPUT_POST, 'course', FILTER_SANITIZE_SPECIAL_CHARS);
  $gender          = filter_input(INPUT_POST, 'gender', FILTER_SANITIZE_SPECIAL_CHARS);


  $args = array(
      'check_category'   => array(
                              'filter' => FILTER_VALIDATE_INT,
                              'flags'  => FILTER_REQUIRE_ARRAY,
                             )
  );

  $check_category  = filter_input_array(INPUT_POST, $args);

  unset($errors);

   //名前
   if (!isset($name) || $name === '') {
       $errors['name'] = '名前が入力されていません';
   } elseif (strlen($name)>100) {
       $errors['name'] = '100文字までです';
   }

  //Eメールアドレス
   if(!isset($email) || $email === ''){
       $errors['email'] = 'Eメールアドレスが入力されていません';
   }
   elseif(strlen($email)>=100){
       $errors['email'] = 'Eメールアドレスは100文字以内で入力してください';
   }
   else{
       if (filter_var($email, FILTER_VALIDATE_EMAIL) !== false) {
         //echo '正しいEメールアドレス形式です';
       }
       else{
         $errors['email'] = 'Eメールアドレスの形式が不正です';
       }
   }
    //性別
    if (!isset($gender) || $gender === '') {
    $errors['gender'] = '性別が選択されていません';
    }
    //コース
    if (!isset($course) || $course === '') {
    $errors['course'] = 'コースが選択されていません';
    }

    //tokenチェック
    CsrfValidator::checkToken();

    //errorが無ければ
    //完了画面に遷移
    if(!isset($errors)){

      header('Location: complete.php');


      $courses_arr        = ["1"=>"PHP","2"=>"Ruby","3"=>"JavaScript"];
      $gender_arr         = ["1"=>"男性","2"=>"女性"];
      $check_category_arr = ["1"=>"社会","2"=>"国語","3"=>"数学","4"=>"英語"];

      $to       = 'sachool.sackle@gmail.com';
      $subject  = '【sachool-test】お申し込み完了';
      $message  = 'お名前:'.$name. "\r\n";
      $message .= 'メールアドレス:'.$email. "\r\n";
      $message .= 'コース：'.$courses_arr[$course]. "\r\n";
      $message .= '性別：'.$gender_arr[$gender]. "\r\n";

      if(isset($check_category)){
        $message .= '興味ある分野：'. "\r\n";
        foreach ($check_category as $key => $value) {
          $message .= '・'.$check_category_arr[$key]. "\r\n";
        }
      }

      $headers = 'From: info@sachool.jp' . "\r\n" .
          'Reply-To: info@sachool.jp' . "\r\n" .
          'X-Mailer: PHP/' . phpversion();

      mail($to, $subject, $message, $headers);
      exit(1);
    }
}
//GETでアクセス
else{
    CsrfValidator::setToken();
}
//print_r($check_category);
//echo $check_category['check_category'][2];
?>
<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>お申し込みフォーム</title>

    <!-- Bootstrap -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
    .row {
        margin-right: 0px;
        margin-left: 0px;
    }
    </style>

  </head>
  <body>
    <div class="container">
      <h2>お申し込みフォーム</h2>

      <?php if (isset($errors)): ?>
        <div class="alert alert-danger" role="alert">
          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
          <span class="sr-only">Error:</span>
          入力エラーがあります。
        </div>
      <?php endif; ?>

      <form action="./" method="post">

          <div class="form-group row <?php if (isset($errors['course'])): ?>has-error has-feedback<?php endif; ?>">
            <label for="inputName" class="col-sm-2 form-control-label">コース</label>
                <div class="col-sm-10">
                  <select class="form-control" name="course">
                    <option value="">------------------</option>
                    <option value="1" <?php if($course == 1): ?>selected="selected"<?php endif; ?>>PHP</option>
                    <option value="2" <?php if($course == 2): ?>selected="selected"<?php endif; ?>>Ruby</option>
                    <option value="3" <?php if($course == 3): ?>selected="selected"<?php endif; ?>>JavaScript</option>
                  </select>
                  <?php if (isset($errors['course'])): ?>
                    <span class="help-block"><?=$errors['course'];?></span>
                  <?php endif; ?>
              </div>
          </div>

          <div class="form-group row <?php if (isset($errors['name'])): ?>has-error has-feedback<?php endif; ?>">
            <label for="inputName" class="col-sm-2 form-control-label">名前</label>
            <div class="col-sm-10">
              <input type="text" name="name" value="<?=$name;?>" class="form-control" id="inputName" placeholder="名前">
              <?php if (isset($errors['name'])): ?>
                <span class="help-block"><?=$errors['name'];?></span>
              <?php endif; ?>
            </div>
          </div>

          <div class="form-group row <?php if (isset($errors['email'])): ?>has-error has-feedback<?php endif; ?>">
            <label for="inputEmail" class="col-sm-2 form-control-label">メールアドレス</label>
            <div class="col-sm-10">
              <input type="text" name="email" value="<?=$email;?>" class="form-control" id="inputEmail" placeholder="Email">
              <?php if (isset($errors['email'])): ?>
                <span class="help-block"><?=$errors['email'];?></span>
              <?php endif; ?>
            </div>
          </div>

        <div class="form-group row　<?php if (isset($errors['gender'])): ?>has-error has-feedback<?php endif; ?>">
          <label class="col-sm-2 form-control-label">性別</label>
          <div class="col-sm-10">
            <div class="radio">
              <label>
                <input type="radio" name="gender" id="gridRadios1" value="1" <?php if($gender == 1): ?>checked="checked"<?php endif; ?>>
                男性
              </label>
            </div>
            <div class="radio">
              <label>
                <input type="radio" name="gender" id="gridRadios2" value="2" <?php if($gender == 2): ?>checked="checked"<?php endif; ?>>
                女性
              </label>
            </div>
            <?php if (isset($errors['gender'])): ?>
              <span class="help-block" style="color:#a94442;"><?=$errors['gender'];?></span>
            <?php endif; ?>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-2">興味のある分野</label>
          <div class="col-sm-10">
            <div class="checkbox">
                <label>
                    <input type="checkbox" name="check_category[1]" value="1" <?php if(isset($check_category['check_category'][1])): ?>checked="checked"<?php endif; ?>> 社会
                </label>
                <label>
                    <input type="checkbox" name="check_category[2]" value="1" <?php if(isset($check_category['check_category'][2])): ?>checked="checked"<?php endif; ?>> 国語
                </label>
                <label>
                    <input type="checkbox" name="check_category[3]" value="1" <?php if(isset($check_category['check_category'][3])): ?>checked="checked"<?php endif; ?>> 数学
                </label>
                <label>
                    <input type="checkbox" name="check_category[4]" value="1" <?php if(isset($check_category['check_category'][4])): ?>checked="checked"<?php endif; ?>> 英語
                </label>
            </div>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" class="btn btn-default">送信する</button>
              <input type="hidden" name="token" value="<?=CsrfValidator::h($_SESSION['token']);?>">
          </div>
        </div>
      </form>


    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  </body>
</html>
