<?php
$file = 'blog.txt';
// ファイルに追加します
$content = "Hello, Today is fine.\n";
// 中身をファイルに書き出します。
// FILE_APPEND フラグはファイルの最後に追記することを表し、
// LOCK_EX フラグは他の人が同時にファイルに書き込めないことを表します。
file_put_contents($file, $content, FILE_APPEND | LOCK_EX);

//読み込み
$file = file_get_contents('./blog.txt', true);
echo $file;




// PHP 5 以降での例
$handle = fopen("./readme.txt", "rb");
$contents = stream_get_contents($handle);
fclose($handle);
echo $contents;
?>
