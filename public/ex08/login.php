<?php
session_start();

//すでにログインしている場合は、マイページ遷移
if($_SESSION['__login_ok'] === TRUE){
    header('Location: mypage.php');
    exit(1);
}

if($_SERVER["REQUEST_METHOD"] == "POST"){
    //POST値の受け取り
    $login_id = $_POST['login_id'];
    $password  = $_POST['password'];

    unset($errors);

    //ログインID
    if (!isset($login_id) || $login_id === '') {
        $errors['login_id'] = 'ログインIDが入力されていません';
    } elseif (strlen($name)>100) {
        $errors['login_id'] = '100文字までです';
    }

    //パスワード
    if(!isset($password) || $password === ''){
        $errors['password'] = 'パスワードが入力されていません';
    }
    elseif(strlen($email)>=100){
        $errors['password'] = 'パスワードは100文字以内で入力してください';
    }

    //ログインIDとパスワードが入力済
    if(isset($login_id) && isset($password)){
        /*
         *http://qiita.com/fantm21/items/603cbabf2e78cb08133e
         */
        $url = "./user.json";
        $json = file_get_contents($url);
        $json = mb_convert_encoding($json, 'UTF8', 'ASCII,JIS,UTF-8,EUC-JP,SJIS-WIN');
        $user_data = json_decode($json,true);

        //jsonファイルと比較
        if($login_id != $user_data['login_id'] || $password != $user_data['password']){
            $errors['password'] = '　ログインIDまたはパスワードが違います';
        }
        else{//ログイン成功
            $_SESSION['__login_id'] = $login_id;
            $_SESSION['__login_ok'] = TRUE;
        }
    }
    //errorが無ければ
    //mypageに遷移
    if(!isset($errors)){
        header('Location: mypage.php');
        exit(1);
    }
}
?>
 <!DOCTYPE html>
 <html lang="ja">
 <body>

 <h2>ユーザログイン</h2>
 <?php if (isset($errors)): ?>
   <div class="alert alert-danger" role="alert">
     <span style="color:red;"><?=$errors['password'];?></span>
   </div>
 <?php endif; ?>
 <div>
   <form action="login.php" method="post">
   <div><p>ID</p><input type="text" name="login_id" value=""></div>
   <div><p>PW</p><input type="password" name="password"></div>
   <div><p><input type="checkbox" name="keep_id">ログインIDを保持する</p></div>
   <input type="submit" name="" value="ログイン">
   </form>

 </div>
 </body>
 </html>
