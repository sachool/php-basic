<?php
session_start();
//すでにログインしていない場合は、ログイン画面に遷移
if($_SESSION['__login_ok'] !== TRUE){
    header('Location: login.php');
    exit(1);
}
?>
<!DOCTYPE html>
<html lang="ja">
<body>
<h2>mypage-<?=$_SESSION['__login_id']?>さん</h2>
ログインできました。
<ul>
    <li>・ログインできる人編集</li>
    <li>・<a href="logout.php">ログアウト</a></li>
</ul>
</body>
</html>
