<?php
session_start();
include_once "../csrf.php";
$db_user = 'root';
$db_pass = '';

//POSTでアクセス
if($_SERVER["REQUEST_METHOD"] == "POST"){
  //POST値の受け取り
  $login_id           = filter_input(INPUT_POST, 'login_id', FILTER_SANITIZE_SPECIAL_CHARS);
  $login_pw           = filter_input(INPUT_POST, 'login_password', FILTER_SANITIZE_SPECIAL_CHARS);
  $keep_id            = filter_input(INPUT_POST, 'keep_id', FILTER_SANITIZE_SPECIAL_CHARS);
  unset($errors);

   //ログインID
   if (!isset($login_id) || $login_id === '') {
       $errors['login_id'] = 'ログインIDが入力されていません';
   } elseif (strlen($login_id)>20) {
       $errors['login_id'] = '20文字までです';
   } elseif (!preg_match("/^[a-zA-Z0-9]+$/", $login_id)) {
       $errors['login_id'] = '半角英数で入力してください';
   }

    //パスワード
    if(!isset($login_pw) || $login_pw === ''){
        $errors['login_password'] = 'パスワードが入力されていません';
    } elseif (strlen($login_pw)>20) {
        $errors['login_password'] = '20文字までです';
    } elseif (!preg_match("/^[a-zA-Z0-9]+$/", $login_pw)) {
        $errors['login_password'] = '半角英数で入力してください';
    }

    //tokenチェック
    CsrfValidator::checkToken();

    //login_idの保存
    if($keep_id){
        setcookie('sachool_login_id',$login_id,time()+3600);//有効期限1時間
    }

    //errorが無ければ
    //ログイン認証を開始
    if(!isset($errors)){

      //ユーザアカウントを取得（DBから）
      try {
         $dbh = new PDO('mysql:host=localhost;dbname=test', $db_user, $db_pass);
         foreach($dbh->query("SELECT * from users where name='".$login_id."' AND password='".$login_pw."'") as $row) {
             $selected_user['id'] = $row['id'];
             $selected_user['password'] = $row['password'];
         }
         //print_r($selected_user);exit(1);
         if(isset($selected_user['id']) && isset($selected_user['password'])){
             //ログイン情報をセッションに格納
             $_SESSION['sachool_login']['login_id'] = $login_id;
             $_SESSION['sachool_login']['status'] = TRUE;

             //マイページに遷移
             header('Location: mypage.php');
             exit(1);

         }
         else{
             echo "ユーザアカウントがみつかりませんでした。<br /><br />";
             echo '<a href="login.php">戻る</a>';
             exit(1);
         }

         $dbh = null;
      } catch (PDOException $e) {
         print "エラー!: " . $e->getMessage() . "<br/>";
         die();
      }


    }
}
//GETでアクセス
else{
    CsrfValidator::setToken();
}
?>


<!DOCTYPE html>
<html lang="ja">
<body>

<h2>ユーザログイン</h2>
<?php if (isset($errors)): ?>
  <div class="alert alert-danger" role="alert">
    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
    <span class="sr-only">Error:</span>
    入力エラーがあります。
  </div>
<?php endif; ?>

<?php if (isset($_GET['status'])): ?>
  <div class="alert alert-danger" role="alert">
    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
    <span class="sr-only">Message:</span>
    <?php if (isset($_GET['status'])): ?><?=htmlspecialchars($_GET['status']);?><?php endif; ?>
  </div>
<?php endif; ?>


<div>
<form action="login.php" method="post">
    <div><p>ID</p><input type="text" name="login_id" value="<?php if (isset($login_id)): ?><?=$login_id;?><?php else: ?><?=$_COOKIE['sachool_login_id'];?><?php endif; ?>">
        <?php if (isset($errors['login_id'])): ?>
          <p class="help-block"><?=$errors['login_id'];?></p>
        <?php endif; ?>
    </div>
    <div><p>PW</p><input type="password" name="login_password">
        <?php if (isset($errors['login_password'])): ?>
          <p class="help-block"><?=$errors['login_password'];?></p>
        <?php endif; ?>
    </div>
    <div><p><input type="checkbox" name="keep_id" value="1">ログインIDを保持する</p></div>
    <input type="submit" name="" value="ログイン">
    <input type="hidden" name="token" value="<?=CsrfValidator::h($_SESSION['token']);?>">
</form>
</div>
</body>
</html>
