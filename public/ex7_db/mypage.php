<?php
session_start();
include_once "../csrf.php";

if(!isset($_SESSION['sachool_login']['status']) && $_SESSION['sachool_login']['status'] != TRUE){
    header("Location:login.php?status=ログインしてください.");
}

//POSTでアクセス
if($_SERVER["REQUEST_METHOD"] == "POST"){
    //POST値の受け取り
    $login_id           = filter_input(INPUT_POST, 'login_id', FILTER_SANITIZE_SPECIAL_CHARS);
    $login_pw           = filter_input(INPUT_POST, 'login_password', FILTER_SANITIZE_SPECIAL_CHARS);
    $update             = filter_input(INPUT_POST, 'update', FILTER_SANITIZE_SPECIAL_CHARS);

    if($update == 'up-account'){
        $file = 'user.txt';
        $new_account = $login_id.":".$login_pw;
        file_put_contents($file, $new_account, LOCK_EX);
        $_SESSION['sachool_login']['login_id'] = $login_id;//ログインID書き換え
        header("Location:mypage.php?status=account updated.");
    }
}
//GETでアクセス
else{
    CsrfValidator::setToken();
}

//アカウント情報の読み込み
$account = file_get_contents('./user.txt', true);
if($account){
    list($read_id,$read_password) = explode(":",$account);
}



$json = file_get_contents('./user.json', true);
//$json = mb_convert_encoding($json, 'UTF8', 'ASCII,JIS,UTF-8,EUC-JP,SJIS-WIN');
$arr = json_decode($json,true);
//print_r($arr);




?>
<!DOCTYPE html>
<html lang="ja">
<body>

<h2>マイページ</h2>

<p><?php if (isset($_GET['status'])): ?><?=htmlspecialchars($_GET['status']);?><?php endif; ?></p>

<div>
<h3>ようこそ<?=$_SESSION['sachool_login']['login_id'];?>さん</h3>
</div>
<form action="mypage.php" method="post">
<div><p><label>ログインID</label><input type="text" name="login_id" value="<?=trim($read_id);?>"></p></div>
<div><p><label>パスワード</label><input type="text" name="login_password" value="<?=trim($read_password);?>"></p></div>
<input type="submit" value="送信する"><br /><br /><br />
<input type="hidden" name="update" value="up-account">
<input type="hidden" name="token" value="<?=CsrfValidator::h($_SESSION['token']);?>">
</form>

<div><a href="logout.php">ログアウト</a></div>
</body>
</html>
